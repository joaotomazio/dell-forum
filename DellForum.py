from random import randrange
from flask import Flask, render_template
import csv

app: Flask = Flask(__name__, template_folder='./')
global data

@app.route('/', methods=['GET'])
def show_list():
    global data
    with open("DellForum.csv") as file:
        csv_reader = csv.reader(file)
        data = list(csv_reader)
        return render_template("index.html")

@app.route('/random', methods=['GET'])
def random():
    global data
    r1 = randrange(1, len(data))
    return render_template("random.html", winner=data[r1][0])

if __name__ == '__main__':
    app.run(host='127.0.0.1', port=8080)
